export const quiz = {
  topic: "English",
  level: "",
  totalQuestions: 20,
  perQuestionScore: 21,
  questions: [
    {
      question:
        "  In my living room I have _____ sofa, _____ two chairs, and _____ wall unit. Which of the following variants must be used in the blanks in the sentence above?",
      choices: [",a,a", "a, , ", "a,a,", "a,__,a"],
      type: "English",
      correctAnswer: "a,__,a",
    },
    {
      question:
        "Summer is hot, But winter is cold. Sugar is sweet, But lemon is _____.",
      choices: ["rich", "yellow", "sharp", "sour"],
      type: "English",
      correctAnswer: "sour",
    },
    {
      question:
        "Please, sit down and feel yourself _____, while I make some tea. Which of the following variants best fits the blank in the sentence above?",
      choices: ["at home", "at school", "in a park", "in an office"],
      type: "English",
      correctAnswer: "at home",
    },
    {
      question: "The slogan of a good student is",
      choices: [
        "The more you study, the better you learn.",
        "The less you study, the better you learn.",
        "The better you study, the worse you learn.",
        "The worse you study, the better you learn.",
      ],
      type: "English",
      correctAnswer: "The more you study, the better you learn.",
    },
    {
      question: "Many _____ come to Kyrgyzstan in summer. Which of the following variants must be used in the blank in the sentence above?",
      choices: ["visits", "visit","visitors","visiting"],
      type: "English",
      correctAnswer: "visitors",
    },
    {
      question: "The girls were happy to be at their graduation party, _____ they realized how they would miss the school.Which of the following variants must be used in the blank in the sentence above?",
      choices: ["whoever", "or","at the same time","because"],
      type: "English",
      correctAnswer: "because",
    },
    {
      question:"A: Are you doing anything tomorrow? B: No, nothing special.A: Shall we go to the football match then?B: No, I don’t really like football. Let’s go to the cinema. A: Brilliant idea. The speakers will most probably",
      choices: ["go to the football match", "stay home","go to the movie","watch the football match at home"],
      type: "English",
      correctAnswer: "go to the movie",
    },
    {
      question: "Ostriches can’t fly, and _____ penguins. Which of the following variants must be used in the blank in the sentence above?",
      choices: ["neither can","neither can’t", "so can","can’t"],
      type: "English",
      correctAnswer: "neither can",
    },
    {
      question: "Danger, do not enter, emergency, fire exit, first aid, harmful, no trespassing...Which of the following categories do the above given words belong to?",
      choices: ["Health words","Safety words","Family words","Calendar words"],
      type: "English",
      correctAnswer: "Safety words",
    },
    {
      question: "– Jane? Where are you?– Hi, Mark. I am at the station. I _____ for the train.Which of the following variants must be used in the blank in the sentence above?",
      choices: ["wait", "am waiting","have waited", "waited"],
      type: "English",
      correctAnswer: "am waiting",
    },
    {
      question: "– Yes, we live in the same building.Which of the following variants is the correct question to the answer above?",
      choices: ["Where does Emma live?","Do you know Emma?","Who lives in the same building?","Why does Emma live in this building?",],
      type: "English",
      correctAnswer: "Do you know Emma?",
    },
    {
      question: "The horse jumped over the fence and _____ away. Which of the following variants must be used in the blank in the sentence above?",
      choices: ["ran","was running","had run","had been running"],
      type: "English",
      correctAnswer: "ran",
    },
    {
      question: "Which of the following variants must be used in the blanks in the sentence above? We _____ the boat to the dock so it wouldn’t go out when the ____ came in.",
      choices: ["tied, tide","tide, tide","tied, tied","tide, tied"],
      type: "English",
      correctAnswer: "tied, tide",
    },
    {
      question: "– I rang at one o’clock, but you were not at your office.– No, I _____ lunch.Which of the following variants must be used in the blank in the sentence above?",
      choices: ["had","am having","was having","have had",],
      type: "English",
      correctAnswer: "was having",
    },
    {
      question: "A: Why didn’t you go to the movie with John?B: He _____ me to see the film that I _____ already.Which of the following variants must be used in the blanks in the sentence above?",
      choices: ["invites, / see","invited, / has seen","has invited, / see","invited, / had seen"],
      type: "English",
      correctAnswer: "invited, / had seen",
    },
    {
      question: "Nurdin: Ever since I met Asel, I haven’t been able to think about other girls. Askar: When I lived in Bishkek I loved Asel, but she didn’t love me.David: I admired Asel when I studied at the University, but we never dated. Misha: I lived in Bishkek for two years and I used to work with Asel.Which of the boys mentioned above is still in love with Asel?",
      choices: ["Nurdin","Askar","David","Misha"],
      type: "English",
      correctAnswer: "Nurdin",
    },
    {
      question: "A famous proverb says: “People have one life, but a cat has nine _____”.Which of the following variants must be used in the blank in the sentence above?",
      choices: ["life","lifes","live","lives"],
      type: "English",
      correctAnswer: "lives",
    },
    {
      question: "The weather was _____ on the day of the graduation ball.Students respect _____ teachers.Which of the following variants must be used in both blanks in the sentences above?",
      choices: ["difficult","nasty","expensive","fair"],
      type: "English",
      correctAnswer: "fair",
    },
    {
      question: "There was a great choice of cheese at the shop. _____ were offered twenty kinds of cheese to taste. Which of the following variants must be used in the blank in the sentence above?",
      choices: ["Us","To us","We","For us",],
      type: "English",
      correctAnswer: "We",
    },
    {
      question: "A: You’ve been out in the garden working in the hot sun since early morning. You _____ be thirsty.",
      choices: ["should","have to","ought to","must"],
      type: "English",
      correctAnswer: "must",
    },
  ],
};
